import avatar from "./assets/images/48.jpg";
import style from "./App.module.css"

function App() {
  return (
    <div className={style.dcContainer}>
      <div>
        <img className={style.dcAvatar} src={avatar} alt="avatar" />
      </div>
      <div className={style.dcQuote}>
        This is one of the best developer blogs on the planet! i read it daily to improve my skills.
      </div>
      <div>
        <span className={style.dcName}>
          Tammy Stevens &nbsp;
        </span>
        <span>
          . &nbsp;Front End Developer
        </span>
      </div>
    </div>
  );
}

export default App;
